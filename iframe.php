<!DOCTYPE html>
<html class="framestyle"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- A minimal Flowplayer setup to get you started -->
  

	<!-- 
		include flowplayer JavaScript file that does  
		Flash embedding and provides the Flowplayer API.
	-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="./flowplayer-3.2.13.min.js"></script>
	<script type="text/javascript" src="./flowplayer.playlist-3.2.11.min.js"></script>
	
	<!-- some minimal styling, can be removed -->
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="videojs/video-js.min.css">
	
	<!-- page title -->
	<title>Lecteur vidéo Streaming UTC</title>

</head><body class="framestyle">

<?php require_once("core.php"); ?>
</body></html>
