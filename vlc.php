<?php
require_once('functions.php');

$streamname='demo';
if (array_key_exists('stream',$_GET) && !empty($_GET['stream'])) {
	$streamname=cleanstream($_GET['stream']);
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Voir la diffusion avec VLC sur mobile</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
body {
	font-size:1.3em;
}
p, li, ul {
	margin-bottom:1em;
}
ul.vertical {
	list-style-type: none;
}
ul.vertical li {
	display:inline-block;
	text-align:center;
}
</style>
</head>
<body>
<h1>Voir la diffusion avec VLC</h1>
<p>En cas de problème de lecture, vous pouvez essayer d'ouvrir le flux vidéo dans l'application VLC.</p>
<h2>Pour mobile (android ou iOS ; smartphone ou tablette)</h2>
<p>Commencez par installer VLC sur votre appareil :</p>
<ul class="vertical"><li><a href="https://play.google.com/store/apps/details?id=org.videolan.vlc&amp;hl=fr"><figure><img src="img/android-small.png" alt=""><figcaption>VLC pour android</figcaption></figure></a></li><li>
<a href="https://itunes.apple.com/fr/app/vlc-for-ios/id650377962?mt=8"><figure><img src="img/apple-small.png" alt=""><figcaption>VLC pour iOS</figcaption></figure></a></li></ul>
<p>Puis suivez ce lien pour afficher le flux :</p>
<ul><li><a href="rtmp://streaming.utc.fr:1935/conf480p/<?php echo $streamname; ?>">Ouvrir le flux video dans VLC mobile</a></li></ul>

<p>Si la diffusion est interompue, n'hesitez pas a relancer le lien par cette page.
</p></body>
</html>
