<!doctype html>
<html lang="fr">
<head>
<!-- A minimal Flowplayer setup to get you started -->
	<meta charset="utf-8">
	<!--meta name="viewport" content="width=900, initial-scale=1.0"!-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- 
		include flowplayer JavaScript file that does  
		Flash embedding and provides the Flowplayer API.
	-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="./flowplayer-3.2.13.min.js"></script>
	<script type="text/javascript" src="./flowplayer.playlist-3.2.11.min.js"></script>
	
	<!-- some minimal styling, can be removed -->
	<link rel="stylesheet" type="text/css" href="style.css">
	
	<!-- page title -->
	<title>Streaming vidéo UTC - aide</title>
<style>
table td, table th {
border:1px solid black;
}
table {
border-collapse: collapse;
}
</style>

</head><body class="contentpage">

	<div>
<h1>Aide à l'utilisation du streaming</h1>
<h2>Si cela ne marche pas</h2>
<ul>
<li>Attendez quelques secondes après avoir cliqué sur lecture, pour que le lecteur ai le temps de charger le flux vidéo.</li>
<li>Rafraichissez la page.</li>
<li>Essayez une autre qualité ou une autre techno de lecture (boutons en dessous de la vidéo).</li>
<li>Essayez avec un autre appareil ou une autre connexion internet.</li>
<li>Vérifiez l'heure de la diffusion de l'événement, s'il n'a pas encore commencé aucun flux n'est envoyé.</li>
</ul>
<h2>Compatibilité</h2>
<p>Ce lecteur vidéo fonctionne dans beaucoup de situations.</p>

<p>Cependant, si vous rencontrez des problèmes, nous vous proposons de vérifier a partir des informations suivantes si vous disposez d'un appareil compatible :</p>
<table>
<tr>
	<th>Technologie</th>
	<th>Navigateurs</th>
	<th>Delais</th>
	<th>Autres contraintes</th>
</tr>
<tr>
	<td>HTML 5</td>
	<td>Navigateurs modernes seulement (FireFox &gt;= 47 ou google chrome &gt;= 53)</td>
	<td>15s</td>
	<td>Lecteur vidéo par défaut qui fonctionne bien dans la majorité des contextes.</td>
</tr>
<tr>
	<td>Ordi (flash)</td>
	<td>Tous avec plugin flash, ou lecteur flash intégré à google chrome</td>
	<td>6s</td>
	<td>Dans de rares circonstances, ne fonctionne pas sur certains réseaux spécifiques (si le port RTMP/1935 est bloqué)</td>
</tr>
<tr>
	<td>Mobile</td>
	<td>Android &gt;= 4.1 (avec google chrome mobile) ou iOS &gt;= 8.0</td>
	<td>15s</td>
	<td></td>
</tr>
<tr>
	<td>VLC</td>
	<td>Android &gt;= 4.1 (avec google chrome mobile) ou iOS &gt;= 8.0, avec l'application VLC</td>
	<td>6s</td>
	<td>Dans de rares circonstances, ne fonctionne pas sur certains réseaux spécifiques (si le port RTMP/1935 est bloqué)</td>
</tr>
</table>
<h2>Contacts</h2>
<p>En cas de problème technique spécifique à la lecture des vidéos sur cet espace de streaming, contactez <a href="mailto:stephane.poinsart@utc.fr">stephane.poinsart@utc.fr</a></p>
</div>

</body></html>
