<?php
$haveapcu=function_exists('apcu_store') && function_exists('apcu_fetch');
if (! $haveapcu) {
	header('HTTP/1.1 500 Internal Server Error');
	echo "APCU php module not installed on server";
	exit;
}


if (array_key_exists('stream',$_REQUEST))
	$streamname=$_REQUEST['stream'];
if (!isset($streamname))
	$streamname="demo";

$apcservicemessage="servicemessage_$streamname";
$servicemessage=apc_fetch($apcservicemessage);
if (!$servicemessage) {
	$servicemessage="";
}
$apconline="online_$streamname";
$online=apc_fetch($apconline);
if (!$online) {
	$online=false;
}


if (array_key_exists("stream_$streamname", $_COOKIE)) {
	$clientid=$_COOKIE["stream_$streamname"];
} else {
	if (array_key_exists("cid", $_GET) && ctype_alnum($_GET["cid"]))
		$clientid=$_GET["cid"];
	else
		$clientid=chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90));
}
setcookie("stream_$streamname", $clientid,time()+3600);

$usercount=1;

function clientcount() {
	global $streamname, $clientid;
	$apcclientlist="clientlist_${streamname}_";
	$apcclient="clientlist_${streamname}_${clientid}";

	apc_store($apcclient, time(), 120);

	$iter = new APCIterator('user',"/^$apcclientlist/", APC_ITER_ALL, 1000);
//	foreach ($iter as $item) {
//		echo "|".$item['key']."-".$item['value']."|<br>";
//	}
	$usercount = iterator_count($iter);

	return $usercount;
}
$usercount=clientcount();


echo json_encode(array("usercount"=>$usercount,"servicemessage"=>$servicemessage, "online"=>$online, "cid"=>$clientid));
?>
