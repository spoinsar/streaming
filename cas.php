<?php

// Full Hostname of your CAS Server
$cas_host = 'cas.utc.fr';
// Context of the CAS Server
$cas_context = '/cas';
// Port of your CAS server. Normally for a https server it's 443
$cas_port = 443;

require_once 'phpCAS/CAS.php';

// Enable debugging
phpCAS::setDebug("/tmp/cas.log");

// Initialize phpCAS
phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context);

// For production use set the CA certificate that is the issuer of the cert
// on the CAS server and uncomment the line below
// phpCAS::setCasServerCACert($cas_server_ca_cert_path);

// For quick testing you can disable SSL validation of the CAS server.
// THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION.
// VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL!
phpCAS::setNoCasServerValidation();

//phpCAS::setNoClearTicketsFromUrl();


// force CAS authentication
//phpCAS::forceAuthentication();

// at this step, the user has been authenticated by the CAS server
// and the user's login name can be read with phpCAS::getUser().



// logout if desired
if (isset($_REQUEST['logout'])) {
	phpCAS::logout();
} else {
	phpCAS::forceAuthentication();
	$login=phpCAS::getUser();
	if (!$login) {
		echo "<html><head><title>Erreur de login</title></head><body><h1>Erreur de login</h1><a href=\"./\">Reconnexion</a></body></html>";
		exit();
	}
}

?>
