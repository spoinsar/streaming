<script>
// En cas de changement de l'URL du serveur de streaming : il faut changer le nom de domaine à 2 endroits dans ce fichier !


var queryDict = {}
location.search.substr(1).split("&").forEach(function(item) {queryDict[item.split("=")[0]] = item.split("=")[1]});
var streamname=queryDict['stream'];
if (!streamname) {
	streamname="demo";
}
var tech=queryDict['tech'];
if (!tech) {
	tech="techexp";
//	if (function(a,b){try{a=new ActiveXObject(a+b+'.'+a+b)}catch(e){a=navigator.plugins[a+' '+b]}return!!a}('Shockwave','Flash')) {
//		tech="techexp";
//	} else {
//		tech="techmobile";
//	}
}
</script>
 
<?php
require_once('functions.php');

$streamname='demo';
if (array_key_exists('stream',$_GET) && !empty($_GET['stream'])) {
	$streamname=cleanstream($_GET['stream']);
}
?>
<div id="fullpage">
	<div id="servicemessage"></div>
	<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
	<div id="videocontainer">
	<div id="flowplayercontainer"><a
		 style="display:block;width:854px;height:480px;margin:0px;margin-bottom:8px;"  
		 id="flowplayer"> 
	</a></div>
	<video id="hlsplayer" style="display:block;width:427px;height:240px;margin:0px;margin-bottom:8px;" controls>
		<source src="" id="hlssource" type="application/x-mpegURL"></source>
	</video>
	<div id="videojscontainer">
		<video id="videojsplayer" class="video-js vjs-default-skin vjs-big-play-centered" data-setup='{"language":"frutc"}' controls>
			<source src="https://streaming.utc.fr/480p/hls-<?php echo $streamname ?>.m3u8" type="application/x-mpegURL">
		</video>
	</div>
	<script src="videojs/video.js"></script>
	<script src="videojs/lang/frutc.js"></script>
	<script src="videojs/videojs-contrib-hls.min.js"></script>
	<!-- this will install flowplayer inside previous A- tag. -->
	<!-- single playlist entry as an "template" -->
	</div>
	<div id="paramsline">
		<span class="displayfield"><span class="label"><a href="aide.php" target="_blank">Aide</a></span></span> 
		<span class="leftline"><span class="label">Qualité :</span> <span class="clips"><span id="quality"></span> <a href="${url}">${title}</a> </span></span>
		<span class="centerline"><span class="label">Techno :</span>
		<span id="tech">
			<a href="#" id="techexp">HTML5</a>
			<a href="#" id="techdesktop">Ordi (flash)</a>
			<a href="#" id="techmobile">Mobile (HLS)</a>
			<a href="vlc.php?stream=<?php echo $streamname ?>" id="techvlc" target="_blank">VLC</a>
		</span></span>
		<span class="rightline"><span class="displayfield" title="Nombre de webospectateurs qui regardent actuellement la page"><span class="label"><img alt="Webospectateur" src="img/user.png"></span> <span id="usercount"></span></span> <span id="online" class="displayfield"></span></span>
	</div>
	<script>

var servername='streaming.utc.fr';

$f("flowplayer", "./flowplayer-3.2.18.swf", {
 
    clip: {
        live: true,
        provider: 'rtmp',
	autoPlay: false,
	autoBuffering: false
    },

playlist: [
	{
		netConnectionUrl: 'rtmp://'+servername+':1935/conf480p',
		url:streamname,
		title:'SD'
	},
	{
		netConnectionUrl: 'rtmp://'+servername+':1935/conf720p',
		url:streamname,
		title:'HD'
	},
	{
		netConnectionUrl: 'rtmp://'+servername+':1935/confuglyp',
		url:streamname,
		title:'Très faible'
	}
],
 
    // streaming plugins are configured under the plugins node
    plugins: {
 
        // here is our rtpm plugin configuration
        rtmp: {
            url: "flowplayer.rtmp-3.2.13.swf",
 
            // netConnectionUrl defines where the streams are found
            netConnectionUrl: 'rtmp://'+servername+':1935/conf480p'
        }
    }
});
$f("flowplayer").playlist("span.clips:first");

var lastmessage="";

var res="480p";

// how many consecutive error
var errorcount=0;

// it's a score to know if we should reload. Positive score = we relaod too many times, skip some
var alreadyreloaded=0

var cid=Math.random().toString(36).substr(2, 5);

var timeoutcontrol;
var lastposition=0;

(function worker() {
	$.ajax({
		url: 'ajax-status.php?stream=<?php echo $streamname; ?>&cid='+cid,
		success: function(data) {
			window.clearTimeout(window.timeoutcontrol);
			var obj=$.parseJSON(data);
			$('#usercount').html(obj.usercount);
			if (obj.online) {
				$('#online').html('<span title="Une équipe est actuellement en train de diffuser l\'évènement"><img alt="" src="img/online.png""> Live</span>');
			} else {
				$('#online').html('<span title="Aucune diffusion en cours sur cette adresse"><img alt="" src="img/offline.png"> Offline</span>');
			}
			if (window.lastmessage!=obj.servicemessage) {
				$('#servicemessage').html(obj.servicemessage);
				window.lastmessage=obj.servicemessage;
			}

			// for how many iteration we can ignore this error (network error = bad, low buffer = meh)
			var errorsla=0;
			if (window.videojsplayer && !window.videojsplayer.paused() && (window.lastposition+0.01)>=window.videojsplayer.currentTime() ) {
				// network state = network error = very bad, but often not triggered
				// ready state = find if buffer not filled = probably bad. Can also be bad connection ?
				// time not increasing when not paused = also very bad
				if (window.videojsplayer.networkState()==3 || typeof window.videojsplayer.networkState()==='undefined' || window.videojsplayer.readyState()==1) {
					errorsla=1;
					window.errorcount++;
				} else if (window.videojsplayer.readyState()==2) {
					errorsla=2;
					window.errorcount++;
				} else {
					window.errorcount=0;
				}
			} else {
				window.errorcount=0;
			}
			window.lastposition=window.videojsplayer.currentTime();
			
			console.log("net status: " + window.videojsplayer.networkState() + "; ispaused"+ window.videojsplayer.paused()
			  	+ " ; readystate: " + window.videojsplayer.readyState() + " ; seeking " +  window.videojsplayer.seeking()+", error state: "+window.videojsplayer.error()+", time: "+window.videojsplayer.currentTime()+", errorcount: "+window.errorcount+", errorsla: "+errorsla+", alreadyreloaded score: "+ window.alreadyreloaded);
			// active code : reload the player if too many errors
			if (errorsla>0 && errorcount>=errorsla && obj.online) {
				if (window.alreadyreloaded<0) {
					videojspaused=window.videojsplayer.paused();
					window.videojsplayer.reset();
					window.videojsplayer.src({type: "application/x-mpegURL", src: "https://"+window.servername+"/"+window.res+"/hls-"+window.streamname+".m3u8"});
					window.videojsplayer.load();
					if (!videojspaused)
						window.videojsplayer.play();
console.log("force reload player: "+"https://"+window.servername+"/"+window.res+"/hls-"+window.streamname+".m3u8");
				}
//				if (!videojspaused)
//					window.videojsplayer.play();
				if (window.alreadyreloaded<-5)
					window.alreadyreloaded=-5;
				window.alreadyreloaded+=4;
				if (window.alreadyreloaded>10)
					window.alreadyreloaded=10;
			} else {
				window.alreadyreloaded--;
			}
 
			// Schedule the next request when the current one's complete
			// very fast in case of error
			if (errorsla>0 && window.alreadyreloaded<0) {
				if (obj.usercount>100)
					window.timeoutcontrol = setTimeout(worker, 10000);
				else
					window.timeoutcontrol = setTimeout(worker, 4000);
console.log("speed up next load");
			// more or lest fast depending on the server load
			} else if (obj.usercount>150) {
				window.timeoutcontrol = setTimeout(worker, 30000);
			} else if (obj.usercount>80) {
				window.timeoutcontrol = setTimeout(worker, 10000);
			} else {
				window.timeoutcontrol = setTimeout(worker, 5000);
			}
		},
		error: function() {
			window.timeoutcontrol = setTimeout(worker, 10000);
		}
	});
})();

var videojsplayer=null;

$("#hlssource").attr('src', "https://"+servername+"/480p/hls-"+streamname+".m3u8");

$( "#techmobile" ).bind( "click", function() {
	$f("flowplayer").stop();
	$("#flowplayercontainer").hide();
	$("#hlsplayer").get(0).load();
	$("#hlsplayer").show();
	if (videojsplayer)
		videojsplayer.pause();
	$("#videojscontainer").hide();
	$("#techmobile").addClass("playing");
	$("#techdesktop").removeClass("playing");
	$("#techexp").removeClass("playing");
});
$( "#techdesktop" ).bind( "click", function() {
	$("#hlsplayer").get(0).pause();
	$("#hlsplayer").hide();
	$("#flowplayercontainer").show();
	if (videojsplayer)
		videojsplayer.pause();
	$("#videojscontainer").hide();
	$("#techdesktop").addClass("playing");
	$("#techmobile").removeClass("playing");
	$("#techexp").removeClass("playing");
});
$( "#techexp" ).bind( "click", function() {
	$("#hlsplayer").get(0).pause();
	$("#hlsplayer").hide();
	$f("flowplayer").pause();
	$("#flowplayercontainer").hide();
	$("#videojscontainer").show();
	if (!videojsplayer)
		videojsplayer = videojs('videojsplayer');
	$("#techdesktop").removeClass("playing");
	$("#techmobile").removeClass("playing");
	$("#techexp").addClass("playing");
});

sdclip=$(".clips a")[0];
hdclip=$(".clips a")[1];
ldclip=$(".clips a")[2];
$(sdclip).addClass("playing");
$(sdclip).bind("click", function resswitchsd(){resswitch(this, '480p')});
$(hdclip).bind("click", function resswitchhd(){resswitch(this, '720p')});
$(ldclip).bind("click", function resswitchld(){resswitch(this, 'uglyp')});

$("#"+tech).trigger("click");
if (tech=="techmobile") {
	$(sdclip).trigger("click");
}

videojs.on();

//videojsplayer.reloadSourceOnError();

function resswitch(obj, newres) {
	paused=false;
	res=newres;
	if (!$("#hlsplayer").get(0).paused) {
		paused=true;
		$("#hlsplayer").get(0).pause();
	}
	$("#hlssource").attr('src', "https://"+servername+"/"+res+"/hls-"+streamname+".m3u8");
	if (videojsplayer) {
		videojspaused=videojsplayer.paused();
		$("#videojsplayer").attr('src', "https://"+servername+"/"+res+"/hls-"+streamname+".m3u8");
		videojsplayer.src({
			src: "https://"+servername+"/"+res+"/hls-"+streamname+".m3u8",
			type: 'application/x-mpegURL'
		});

		if (!videojspaused)
			videojsplayer.play();
	}
	$("#hlsplayer").get(0).load();
	if (paused) {
		$("#hlsplayer").get(0).play();
	}
	$(".clips a").removeClass("playing");
	$(obj).addClass("playing");
}
</script>
</div>
