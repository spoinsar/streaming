<?php
require_once('functions.php');

$haveapcu=function_exists('apcu_store') && function_exists('apcu_fetch');
if (! $haveapcu) {
	header('HTTP/1.1 500 Internal Server Error');
	echo "APCU php module not installed on server";
	exit;
}


if (array_key_exists('stream',$_REQUEST))
	$streamname=cleanstream($_REQUEST['stream']);

if (!isset($streamname)) {
	echo "ERROR: no stream name";
	exit;
}
if (array_key_exists('online',$_REQUEST))
	$online=($_REQUEST['online']=='1');
if (!isset($online)) {
	echo "ERROR: no online status";
	exit;
}

$apconline="online_$streamname";
if ($online)
	sleep(15);

apc_store($apconline, $online, 36000);
echo "set $apconline $online"
?>
