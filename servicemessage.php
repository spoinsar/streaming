<?php
$haveapcu=function_exists('apc_store') && function_exists('apc_fetch') && ini_get('apc.enabled');
if (!$haveapcu) {
	echo "apcu php module not installed on server";
	exit;
}
if (array_key_exists('stream',$_REQUEST) && array_key_exists('duration',$_REQUEST) && array_key_exists('servicemessage',$_REQUEST)) {
	$stream=$_REQUEST['stream'];
	$duration=$_REQUEST['duration'];
	$servicemessage=$_REQUEST['servicemessage'];
	$apcservicemessage="servicemessage_$stream";
	if ($servicemessage==='-')
		$servicemessage="";
	apc_store($apcservicemessage, $servicemessage, $duration*60);
	$status="Message enregistré !";
}

echo '<?xml version="1.0" encoding="UTF-8" ?>';
?>
<!DOCTYPE html>
<html class="framestyle" style="margin:8px;"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Lecteur vidéo Streaming UTC</title>
</head><body class="framestyle">
<!--a class="logout" href="./?logout=1">Déconnexion <?php echo $login;?></a-->
<h1>Message de service pour le streaming</h1>
<?php
if (isset($status))
	echo "<p class=\"status\">$status</p>";
?>
<br>
<form method="post" action="#">
<div><label for="strea">Nom de stream</label> : <input type="text" name="stream" value="<?php echo (isset($stream)?$stream:"demo"); ?>"/></div>
<div><label for="duration">Durée (minutes)</label> : <input type="text" name="duration" value="<?php echo (isset($duration)?$duration:"120"); ?>"/></div>
<div><label for="servicemessage">Message de service</label> : <textarea name="servicemessage" cols="130" rows="10"><?php echo(isset($servicemessage)?htmlspecialchars($servicemessage):htmlspecialchars("<div class=\"box error\"><span class=\"icon\">&nbsp</span>Le service de streaming est temporairement indisponible, nous travaillons à son rétablissement</div>")); ?></textarea></div>
<input type="submit" value="Envoyer le message sur la page utilisateur">
</form>

<h2>Messages en cours de diffusions</h2>
<p>Les messages ci-dessous sont actifs sur les différentes pages de flux :</p>
<div id="servicemessage">
<?php
foreach (new APCIterator('user', '/servicemessage_\.*/') as $currentmessage) {
	echo "<p><br>$currentmessage[key]</b> :</p>";
	echo "$currentmessage[value]\n";
	$hasmessage=true;
}
if (!isset($hasmessage))
	echo "<p>Pas de messages en cours de diffusion</p>";
?>
</div>

<h2>Exemples</h2>
<p>Message d'erreur générique à copier-coller :</p>
<xmp><div class="box error"><span class="icon"> </span>Le service de streaming est temporairement indisponible, nous travaillons à son rétablissement</div></xmp>
<p>Message d'info générique :</p>
<xmp><div class="box info"><span class="icon"> </span>L'évènement n'a pas encore débuté, la diffusion commencera à XX:YY heures...</div></xmp>
<p>Effacer le message actif :</p>
<pre>-</pre>
</body></html>
