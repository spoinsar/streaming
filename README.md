# Libre Streaming Server/Player stack

## Content
You will find :

* The player (the majority of the files in this package)
  * Compatible with most playing context (see "aide.php")
  * Can display service messages, to warn viewer about errors or to give them informations about the event
  * Display the number of viewers (currently just the number of people who have the page open, even if they paused the stream)
* The server config, based on nginx and it's rtmp module
  * support RTMP input
  * support HLS (mobile or html5, default) or RTMP (flash) output, with 3 different encoding quality
  * Everything is processed in a single ffmpeg command

Server and player pages are currently designed to run on 2 different servers. It's a lightweight stack, manual install only but easy to tinker.

There is currently just the french version of the web player.

## Usage
After server and player setup :

* Stream to the server. You choose the stream name but you must know the other parts of the URL. The target URL should be something like rtmp://streamserver.mydomain.com/CHANGE-THIS-SECRET-URL/streamname . It has been tested successfully for live events with obs studio ( <https://obsproject.com/> ) and wirecast ( <https://www.telestream.net/wirecast/overview.htm> ). An easy test can be done with ffmpeg to feed a static file from your own desktop computer : `ffmpeg -re -i myvideo.mp4 -c:v libx264 -preset veryfast -maxrate 5000k -bufsize 10000k -pix\_fmt yuv420p -g 50 -c:a aac -b:a 160k -ac 2 -ar 44100 -f flv rtmp://streamserver.mydomain.com/CHANGE-THIS-SECRET-URL/demo` . If you have to stream an event from a new location from a remote place, check if they have proper network access with rtmp port open.
* browse to the player URL, and add something like /?stream=streamname  . I.E. : https://webserver.mydomain.com/streaming/?stream=demo


## Streaming server setup
* Hardware : it has been tested on a 4 core VM (over a Xeon E5-2660 v2) + 4Gb ram, and it can run 2 HD or FHD input streams simultaneously (with output as x3 quality x2 stream formats).
* I installed nginx and nginx-rtmp from source, it' not very hard : <https://github.com/arut/nginx-rtmp-module>
* You will also need ffmpeg. Also using source here, but recent ubuntu or debian package should probably work
* You can find the nginx config file in server-config/ directory of this git repository
* You must change the 2 passwords, SSL key to serve the HLS fragments over https, and the hardcoded server URL or file path
* Create the directories to serve the HLS fragments : `mkdir -p /var/www/720p/ /var/www/480p /var/www/uglyp` ; and then give write permission to nginx on those directories (because nginx-rtmp does the encoding)
* You may want to add a cleanup command in your /etc/crontab to purge old HLS video fragments on inactive files :

```
*/2 *   * * *   root    find /var/www/720p/ /var/www/480p /var/www/uglyp -name '*.ts' -mmin +5 -type f -delete
*/2 *   * * *   root    find /var/www/720p/ /var/www/480p /var/www/uglyp -name '*.m3u8' -mmin +2 -type f -delete
```

Could be documented further...


## Player setup (on a php webserver)
* Copy the file on a php 5.5 or php 5.6 webserver (php 7 might work ?)
* Install apcu (php5-apcu) and any other php module that i might have forgotten.
* Change any reference to "streaming.utc.fr" to your own server : grep streaming.utc.fr \*.php
* Create a .htaccess and a .htpasswd file to get some basic security, you can start from there (but change the absolute path of AuthUserFile to suit your needs). Add 2 users : "admin" to control the stream service messages (so only you can change the messages delivered to users), and "statuschange" that you have to reuse in the nginx config file (so only you can set the stream as active or dead).

``` apache
<filesMatch "\.(html|htm|php)$">
  FileETag None
  <ifModule mod_headers.c>
     Header unset ETag
     Header set Cache-Control "max-age=0, no-cache, no-store, must-revalidate"
     Header set Pragma "no-cache"
     Header set Expires "Wed, 11 Jan 1984 05:00:00 GMT"
  </ifModule>
</filesMatch>
<Files "servicemessage.php">
     AuthName "Interface gestionnaire messages streaming (admin)"
     AuthType Basic
     AuthUserFile /usr/src/clipbucket/upload/streaming/.htpasswd
     Require user admin
</Files>
<Files "status-change.php">
     AuthName "Script de gestion de l'etat du stream (statuschange)"
     AuthType Basic
     AuthUserFile /usr/src/clipbucket/upload/streaming/.htpasswd
     Require user statuschange
</Files>
```

## License (GPL v3)
Libre Streaming Server/Player stack
Stéphane Poinsart <<stephane.poinsart@utc.fr>>, Université de Technologie de Compiègne

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Special thanks
Thanks for notifying me of (now fixed) security vulnerabilities :

* DrStache <https://www.openbugbounty.org/reports/579096/>

Third party programs used :

* FFmpeg <https://www.ffmpeg.org/>
* nginx <https://nginx.org/>
* nginx-rtmp <https://github.com/arut/nginx-rtmp-module>
* VideoJS <https://videojs.com/> with HLS plugin <http://videojs.github.io/videojs-contrib-hls/>
* Flowplayer <https://github.com/flowplayer/flowplayer>
* jQuery <https://jquery.com/>

